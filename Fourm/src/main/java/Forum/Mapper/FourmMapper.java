package Forum.Mapper;

import Forum.Pojo.ForumData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface FourmMapper {
    //获取所有的论坛
    List<ForumData> getalltalk();
    //获取用户的帖子
    List<ForumData> getmytalk(String fuid);
    //用户发帖
    Integer addtalk(ForumData forumData);
    //用户编辑自己的帖子
    Integer upmytalk(ForumData forumData);
    //删除帖子
    Integer deltalk(String fid);
}
