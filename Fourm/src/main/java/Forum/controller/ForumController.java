package Forum.controller;

import Forum.Pojo.ForumData;
import Forum.Pojo.ResultData;
import Forum.Service.ServiceIMP.ForumServiceImp;
import com.baomidou.mybatisplus.extension.api.R;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RequestMapping("/Forum")
@RestController
@SpringBootApplication
public class ForumController {
    @Resource
    private ForumServiceImp forumServiceImp;
    //获取所有论坛
    @GetMapping("/getall")
    public ResultData getall(){
        List<ForumData> forumData = forumServiceImp.getalltalk();
        if (forumData != null)
            return new ResultData("200","OK!",forumData);
        else return new ResultData("600","error","");
    }

    @GetMapping("/getmytalk")
    public ResultData getmy(@RequestParam("fuid") String uid){
        //System.out.println(uid);
        List<ForumData> forumData = forumServiceImp.getmytalk(uid);
        if (forumData != null)
            return new ResultData("200","OK!",forumData);
        else return new ResultData("600","error","似乎你还没有发过帖子");
    }
    //发布帖子
    @PostMapping("/addTalk")
    public ResultData add(@RequestBody ForumData forumData){
        Integer codes = forumServiceImp.addtalk(forumData);
        if(codes == 100){
            return new ResultData("200","OK!","添加成功");
        }else {
            return new ResultData("200","error!","请核对你的参数！");
        }
    }
    //编辑帖子
    @PostMapping("/upmytalk")
    public ResultData upmytalk(@RequestBody ForumData forumData) {
        Integer codes = forumServiceImp.upmytalk(forumData);
        if(codes == 100){
            return new ResultData("200","OK!","修改成功");
        }
        else {
            return new ResultData("200","error!","请核对你的参数！");
        }
    }

    //删除帖子
    @GetMapping("/deltalk")
    public ResultData deltalk(@RequestParam("fid") String fid){
        forumServiceImp.deltalk(fid);
        return new ResultData("200","ok","");
    }
}
