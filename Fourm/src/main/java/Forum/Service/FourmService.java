package Forum.Service;

import Forum.Pojo.ForumData;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FourmService {
    List<ForumData> getalltalk();
    //获取用户的帖子
    List<ForumData> getmytalk(String fuid);
    //用户发帖
    Integer addtalk(ForumData forumData);
    //编辑自己发帖
    Integer upmytalk(ForumData forumData);
    //删除帖子
    Integer deltalk(String fid);
}
