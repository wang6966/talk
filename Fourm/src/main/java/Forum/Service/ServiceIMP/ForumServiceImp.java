package Forum.Service.ServiceIMP;

import Forum.Mapper.FourmMapper;
import Forum.Pojo.ForumData;
import Forum.Service.FourmService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service //告诉它你这个是service层，让它可以找的到
@Transactional
public class ForumServiceImp implements FourmService {
    //返回所有的论坛
    @Resource
    FourmMapper fourmMapper; //注解 mapper 层，调用里面的方法

    //拿到    mapper  的数据
    @Override
    public List<ForumData> getalltalk() {
        try{
            List<ForumData> forumData = fourmMapper.getalltalk();
            return forumData; //返回查询的结果
        }catch (Exception e){
            return null;
        }
    }
    //获取用户的帖子
    @Override
    public List<ForumData> getmytalk(String fuid) {
        try{
            List<ForumData> forumData = fourmMapper.getmytalk(fuid);
            return forumData;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public Integer addtalk(ForumData forumData) {
        try{
            fourmMapper.addtalk(forumData);
            return 100;
        }catch (Exception e){
            return -100;
        }
    }

    //更新自己的帖子
    @Override
    public Integer upmytalk(ForumData forumData){
        try{
            fourmMapper.upmytalk(forumData);
            return 100;
        }
        catch (Exception e){
            return null;
        }
    }

    //删除帖子
    @Override
    public Integer deltalk(String fid) {
        try {
            fourmMapper.deltalk(fid);
            return 100;
        }catch (Exception e){
            return -100;
        }
    }


}
