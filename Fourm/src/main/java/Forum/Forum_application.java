package Forum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Forum_application {
    public static void main(String[] args) {
        SpringApplication.run(Forum_application.class,args);
    }
}
