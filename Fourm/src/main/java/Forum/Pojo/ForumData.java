package Forum.Pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data
//等同于所有字段的get个set方法
public class ForumData {
    //按照数据库中的字段，写出数据库中字段的数据类型
    private String ftitle;
    private String fcontent;
    private Date fdata;
    private String fid;
    private String fposter;
    private String fimg;
    private String fuid;

    @Override
    public String toString() {
        return "ForumData{" +
                "ftitle='" + ftitle + '\'' +
                ", fcontent='" + fcontent + '\'' +
                ", fdata=" + fdata +
                ", fid='" + fid + '\'' +
                ", fposter='" + fposter + '\'' +
                ", fimg='" + fimg + '\'' +
                ", fuid='" + fuid + '\'' +
                '}';
    }
}
