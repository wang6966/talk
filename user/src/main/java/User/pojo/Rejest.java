package User.pojo;

import lombok.Data;

@Data
public class Rejest {

    private String uname;
    private String passward;
    private String email;
    private String phone;
    private String usex;
    private String codes;

    @Override
    public String toString() {
        return "Rejest{" +
                "uname='" + uname + '\'' +
                ", passward='" + passward + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", usex='" + usex + '\'' +
                ", codes='" + codes + '\'' +
                '}';
    }
}
