package User.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@Data
@TableName("uforum")
public class Userdata {
    private String uid;

    private String uname;

    private String utype;

    private String avatar;

    private String passward;

    private String email;

    private String phone;

    private String introduct;

    private String usex;


    @Override
    public String toString() {
        return "Userdata{" +
                "uid='" + uid + '\'' +
                ", uname='" + uname + '\'' +
                ", utype='" + utype + '\'' +
                ", avatar='" + avatar + '\'' +
                ", passward='" + passward + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", introduct='" + introduct + '\'' +
                ", usex='" + usex + '\'' +
                '}';
    }
}
