package User.Controller;

import User.Service.MailService;
import User.Service.impl.UserServiceImpl;
import User.pojo.Rejest;
import User.pojo.ResultData;
import User.pojo.Userdata;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RequestMapping("/Test1")
@RestController
@SpringBootApplication
public class UserCon {

    @Resource
    private UserServiceImpl userService;
    @Resource
    private MailService mailService;
//    @RequestMapping("Test")
//    public static void main(String[] args) {
//        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("ac.xml");
//        UserServiceImpl userService = applicationContext.getBean("userService", UserServiceImpl.class);
//        boolean b = userService.login("lzh", "123456");
//        if (b){
//            System.out.println("登陆成功");
//        }else {
//            System.out.println("登录失败");
//        }
//    }


    @RequestMapping("/getall")
    public ResultData getall()
    {
        List<Userdata> userdata = userService.getalluser();
        return new ResultData("200","OK!",userdata);
    }

    @RequestMapping("/login")
        public ResultData login1(@RequestParam("em") String em,
                             @RequestParam("pass") String pass)
    {
        String tips =  userService.login3(em,pass);
        return new ResultData("200","OK!",tips);
    }
    @PostMapping("/enroll")
    public ResultData enroll(@RequestBody Rejest rejest){
        String en = userService.uforum(rejest);
        if(en.equals("注册成功")) return new ResultData("200","OK",en);
        else return new ResultData("400","error",en);
    }

    @GetMapping("/sentcode")
    public ResultData sent(@RequestParam("email") String e){
        String mess = userService.sentcode(e);
        return new ResultData("200","OK",mess);
    }


}
