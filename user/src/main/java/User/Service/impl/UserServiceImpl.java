package User.Service.impl;

import User.Mapper.UserMapper;
import User.Mapper.Users;
import User.Service.MailService;
import User.Service.UserService;
import User.pojo.Rejest;
import User.pojo.Userdata;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private MailService mailService;
    @Resource
    private Users users;
    @Autowired
    RedisTemplate redisTemplate;

    @Override
    public boolean login(String name, String passward) {
        return userMapper.login(name, passward);
    }

    @Override
    public List<Userdata> getalluser() {
        return userMapper.getalluser();

    }

    @Override
    public Userdata login2(String email) {
        return null;
    }

    @Override
    public String login3(String email, String passward) {
        Userdata userdata = userMapper.login2(email);
        String pass = userdata.getPassward();
        if(passward.equals(pass)){
            return "密码正确";
        }else
            return "密码错误";
    }

    //

    //用户注册
    @Override
    public String uforum(Rejest rejest) {
        Userdata userdata = new Userdata();
        LambdaQueryWrapper<Userdata> queryWrapper = new LambdaQueryWrapper<Userdata>();
        queryWrapper.eq(Userdata::getEmail,rejest.getEmail());
        Userdata userdata2 = users.selectOne(queryWrapper);
        if(userdata2 != null)   return "邮箱已经被注册！";

        ValueOperations redis = redisTemplate.opsForValue();
        String hms= (String)redis.get(rejest.getEmail());
        if(!rejest.getCodes().equals(hms))  return "验证码错误！";

        userdata.setAvatar("http://47.99.34.48:9000/fourm/fourm_1687336831623_2023-06-21_640.png");
        userdata.setIntroduct("暂时什么都没有填写");
        userdata.setUname(rejest.getUname());
        //uuid生成8为用户ID
        Integer uuid= UUID.randomUUID().toString().replaceAll("-","").hashCode();
        uuid = uuid < 0 ? -uuid : uuid;//String.hashCode() 值会为空
        String uuid2 = uuid.toString();
        userdata.setUid(uuid2);
        userdata.setPassward(rejest.getPassward());
        userdata.setEmail(rejest.getEmail());
        //userdata.s
        userdata.setPhone(rejest.getPhone());
        userdata.setUtype("普通人员");
        userdata.setUsex(rejest.getUsex());
        userMapper.uforum(userdata);
        return "注册成功";

    }

    @Override
    public String sentcode(String em) {
        //判断邮箱是否被注册
        LambdaQueryWrapper<Userdata> queryWrapper = new LambdaQueryWrapper<Userdata>();
        queryWrapper.eq(Userdata::getEmail,em);
        Userdata userdata = users.selectOne(queryWrapper);
        if(userdata != null)
        {
            return "已经存在用户";
        }
        //产生验证码，并存入redis
        Random random=new Random();
        String sms=String.valueOf(random.nextInt(9999-1000+1)+1000);    //随机验验证码
        ValueOperations redis = redisTemplate.opsForValue();
        redis.set(em,sms,3, TimeUnit.MINUTES);  //有效时间

        //将验证码用邮箱发送
        mailService.sendSimpleMail(em,"青论坛的验证码","你好，你正在注册青论坛相关，验证码为："+sms+",有效时间为3分钟，请在三分钟内完成注册。如果非本人操作，请忽略该邮件。");
        return null;
    }


}
