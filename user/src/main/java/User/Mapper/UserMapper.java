package User.Mapper;

import User.pojo.Userdata;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {
    public boolean login(String name,String passward);

    public List<Userdata> getalluser();

    public Userdata login2(String email);

    public String login3(String email,String passward);

    Integer uforum (Userdata userdata);


}
