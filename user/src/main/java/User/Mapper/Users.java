package User.Mapper;

import User.pojo.Userdata;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@TableName("uforum")
public interface Users extends BaseMapper<Userdata> {

}
